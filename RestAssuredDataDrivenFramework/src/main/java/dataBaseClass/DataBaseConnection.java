package dataBaseClass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class DataBaseConnection {
	
	public void init(){
		try{
			Class.forName("com.mysql.jdbc.Driver"); // loads the driver class
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc", "root", "root"); //creates connection with the database
			Statement st = con.createStatement();// create statement
			ResultSet result = st.executeQuery("select * from location ");//execute the statement and return the result into the ResultSet
			
	while(result.next())//processing the ResultSet
	{
		String zipcode = result.getString("zipcode");
		String city= result.getString("city");
		String state = result.getString("state");
		
		System.out.println(zipcode);
		System.out.println(city);
		System.out.println(state);
	}
	result.close();
	st.close();
	con.close();
		}catch(Exception ex){
			System.out.println("Message " + ex);
		}
		}
}
	


